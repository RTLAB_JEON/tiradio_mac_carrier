#include <ubinos.h>

#if (UBINOS__BSP__BOARD_MODEL == UBINOS__BSP__BOARD_MODEL__STM3221GEVAL)

#include "stm32f2xx_hal_conf_stm3221geval.h"

#elif (UBINOS__BSP__BOARD_MODEL == UBINOS__BSP__BOARD_MODEL__NUCLEOF207ZG)

#include "stm32f2xx_hal_conf_nucleof207zg.h"

#else

#error "Unsupported UBINOS__BSP__BOARD_MODEL"

#endif
