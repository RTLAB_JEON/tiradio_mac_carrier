/*
 * tiradio_carrier.h
 *
 *  Created on: 2019. 11. 21.
 *      Author: chlee
 */

#ifndef TIRADIO_MAC_CARRIER_H_
#define TIRADIO_MAC_CARRIER_H_

#include <stdint.h>

#include <tiradio.h>

//REALY PACKET LEN
#define carrier_ADDR_LEN			6
#define carrier_TYPE_LEN			1
#define carrier_SEQ_NO_LEN		2
#define carrier_DIRECTION_LEN		1
#define carrier_MSG_SIZE_LEN		1
#define carrier_HEADER_LEN		carrier_ADDR_LEN * 2 + carrier_TYPE_LEN + carrier_SEQ_NO_LEN + carrier_DIRECTION_LEN + carrier_MSG_SIZE_LEN
#define carrier_MSG_SIZE_MAX		(TIRADIO_PACKET_PAYLOAD_SIZE_MAX - carrier_HEADER_LEN)

//carrier PACKET INDEX
#define carrier_INDEX_TYPE 				0
#define carrier_INDEX_SRC_ADDR_BASE 		1
#define carrier_INDEX_DST_ADDR_BASE		7
#define carrier_INDEX_SEQ_NO_LOW 			13
#define carrier_INDEX_SEQ_NO_HIGH 		14
#define carrier_INDEX_carrier_DIR 			15
#define carrier_INDEX_MSG_LEN 			16
#define carrier_INDEX_MSG_BASE 			17

#define carrier_MODE_SWITCH_DELAY_MS		5

typedef enum {
	carrier_PACKET_TYPE_GENERAL = 0x01, carrier_PACKET_TYPE_carrier, carrier_PACKET_TYPE_ACK,
} carrier_packet_type_t;

typedef enum {
	carrier_DIR_NONE = 0, carrier_DIR_FORWARD, carrier_DIR_BACKWARD,
} carrier_direction_t;

typedef struct {
	tiradio_t *tiradio;
	uint8_t my_addr[carrier_ADDR_LEN];
	uint8_t addr_unit;
	uint8_t retry_cnt;
	uint8_t retry_timeout;
} tiradio_carrier_config_t;

typedef struct {
	uint8_t type;
	uint8_t src_addr[carrier_ADDR_LEN];
	uint8_t dst_addr[carrier_ADDR_LEN];
	uint16_t seq_no;
	uint8_t direction;
	uint8_t msg_len;
	uint8_t msg[carrier_MSG_SIZE_MAX];
} tiradio_carrier_packet_t;

tiradio_err_t carrier_packet_rx(tiradio_carrier_config_t *carrier_config, tiradio_carrier_packet_t *carrier_packet, uint8_t carrier_msg_len);
tiradio_err_t carrier_packet_tx(tiradio_carrier_config_t *carrier_config, tiradio_carrier_packet_t *carrier_packet);
tiradio_err_t carrier_packet_send_ack(tiradio_carrier_config_t *carrier_config, tiradio_carrier_packet_t *carrier_received_packet);
tiradio_err_t carrier_packet_forward(tiradio_carrier_config_t *carrier_config, tiradio_carrier_packet_t *carrier_received_packet);

#endif /* TIRADIO_MAC_CARRIER_H_ */
