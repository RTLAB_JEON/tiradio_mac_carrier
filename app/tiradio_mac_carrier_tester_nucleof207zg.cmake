set_cache(UBINOS__UBICLIB__EXCLUDE_CLI FALSE BOOL)

set_cache(STM32CUBEF2__BOARD_NAME "STM32F2XX_NUCLEO_144" STRING)

set_cache(STM32CUBEF2__USE_HAL_DRIVER TRUE BOOL)
set_cache(STM32CUBEF2__USE_FULL_LL_DRIVER TRUE BOOL)

#set_cache(UBINOS__BSP__GDBSERVER_HOST "bb2" PATH)
#set_cache(UBINOS__BSP__GDBSERVER_PORT "2331" PATH)

include(${PROJECT_UBINOS_DIR}/config/ubinos_nucleof207zg.cmake)

include(${PROJECT_LIBRARY_DIR}/stm32cubef2_wrapper/config/stm32cubef2.cmake)

include(${PROJECT_LIBRARY_DIR}/tiradio/config/tiradio.cmake)

include(${CMAKE_CURRENT_LIST_DIR}/../config/tiradio_mac_carrier.cmake)

####

set(INCLUDE__APP TRUE)
set(APP__NAME "tiradio_mac_carrier_tester")

get_filename_component(_tmp_source_dir "${CMAKE_CURRENT_LIST_DIR}/tiradio_mac_carrier_tester" ABSOLUTE)

file(GLOB_RECURSE _tmp_sources
    "${_tmp_source_dir}/*.c"
    "${_tmp_source_dir}/*.cpp"
    "${_tmp_source_dir}/*.S"
    "${_tmp_source_dir}/*.s")

set(PROJECT_APP_SOURCES ${PROJECT_APP_SOURCES} ${_tmp_sources})

include_directories(${_tmp_source_dir})
include_directories(${_tmp_source_dir}/arch/arm/cortexm/stm32f2)

