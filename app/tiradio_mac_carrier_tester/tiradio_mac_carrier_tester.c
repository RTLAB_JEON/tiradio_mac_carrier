#include <ubinos.h>

#if (INCLUDE__APP__tiradio_mac_carrier_tester == 1)

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>

#include "tiradio_mac_carrier_tester.h"

//Set to 1 for tx source device
#define carrier_TESTER_TX				0

#define TEST_TX_PERIOD_MS			5000

//Test carrier address
#define TEST_carrier_ADDRESS0			0x00
#define TEST_carrier_ADDRESS1			0x00
#define TEST_carrier_ADDRESS2			0x00
#define TEST_carrier_ADDRESS3			0x00
#define TEST_carrier_ADDRESS4			0x00
#define TEST_carrier_ADDRESS5			0x02

//next addr = my addr + addr unit
#define TEST_carrier_ADDR_UNIT		0x01

//Test msg len
#define TEST_carrier_MSG_LEN			11

#define TEST_carrier_ACK_DELAY_MS		10

//test net packet
uint8_t test_net_my_addr[4] = {0x00, 0x00, 0x00, 0x02};
uint8_t test_net_dst_addr[4] = {0x00, 0x00, 0x00, 0x03};

#define TEST_NET_ADDR_LEN				4

#define TEST_NET_INDEX_PACKET_TYPE			0
#define TEST_NET_INDEX_SRC_ADDR_BASE		1
#define TEST_NET_INDEX_DST_ADDR_BASE		5
#define TEST_NET_INDEX_DATA_BASE			9

enum
{
	TEST_NET_PACKET_TYPE_GENERAL = 1, TEST_NET_PACKET_TYPE_ACK
};
tiradio_t *_g_tiradio = NULL;

tiradio_t _tiradio;

tiradio_test_params_t _tiradio_test_params;

SPI_HandleTypeDef hspi3;

bool rx_state = false;

tickcount_t tickcount_tx;
tickcount_t tickcount_forward;
tickcount_t tickcount_carrier_ack;
tickcount_t tickcount_net_ack;
tickcount_t tickdiff1;
tickcount_t tickdiff2;


static void radio_init(void);

static void root_task(void *arg);

void carrier_test_main(void *arg);
void carrier_test_cmd_main(void *arg);

#if (UBINOS__BSP__BOARD_MODEL == UBINOS__BSP__BOARD_MODEL__STM3221GEVAL)

static void radio_init(void) {
	tiradio_t *tiradio = &_tiradio;
	GPIO_InitTypeDef GPIO_InitStruct;

	////

	__HAL_RCC_GPIOG_CLK_ENABLE();
	__HAL_RCC_GPIOI_CLK_ENABLE();

	GPIO_InitStruct.Pin = GPIO_PIN_6;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_8;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_9;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOI, &GPIO_InitStruct);
	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 2, 0);
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);

	hspi3.Instance = SPIx;
	hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_16;
	hspi3.Init.Direction = SPI_DIRECTION_2LINES;
	hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi3.Init.CRCPolynomial = 7;
	hspi3.Init.DataSize = SPI_DATASIZE_8BIT;
	hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi3.Init.NSS = SPI_NSS_SOFT;
	hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi3.Init.Mode = SPI_MODE_MASTER;
	if (HAL_SPI_Init(&hspi3) != HAL_OK) {
		return;
	}

	////

	tiradio->reset_n.port = GPIOG;
	tiradio->reset_n.pin = GPIO_PIN_6;

	tiradio->cs_n.port = GPIOG;
	tiradio->cs_n.pin = GPIO_PIN_8;

	tiradio->gpio0.port = GPIOI;
	tiradio->gpio0.pin = GPIO_PIN_9;
	tiradio->gpio0.irq_no = EXTI9_5_IRQn;

	tiradio->spi = &hspi3;

	if (tiradio_init(tiradio) != TIRADIO_ERR_OK) {
		return;
	}
	_g_tiradio = tiradio;

	if (tiradio_reset(tiradio) != TIRADIO_ERR_OK) {
		return;
	}
}

#elif (UBINOS__BSP__BOARD_MODEL == UBINOS__BSP__BOARD_MODEL__NUCLEOF207ZG)

static void radio_init(void) {
	tiradio_t *tiradio = &_tiradio;
	GPIO_InitTypeDef GPIO_InitStruct;

	////

	__HAL_RCC_GPIOC_CLK_ENABLE();
	__HAL_RCC_GPIOD_CLK_ENABLE();
	__HAL_RCC_GPIOG_CLK_ENABLE();

	GPIO_InitStruct.Pin = GPIO_PIN_2;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = GPIO_PIN_8;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FAST;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

	HAL_NVIC_DisableIRQ(EXTI2_IRQn);
	GPIO_InitStruct.Pin = GPIO_PIN_2;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
	HAL_NVIC_SetPriority(EXTI2_IRQn, 2, 0);

	HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
	GPIO_InitStruct.Pin = GPIO_PIN_9;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);
	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 2, 0);

	HAL_NVIC_DisableIRQ(EXTI3_IRQn);
	GPIO_InitStruct.Pin = GPIO_PIN_3;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);
	HAL_NVIC_SetPriority(EXTI3_IRQn, 2, 0);

	hspi3.Instance = SPIx;
	//hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_32; // 1 MHz
	hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_8; // 4 MHz
	hspi3.Init.Direction = SPI_DIRECTION_2LINES;
	hspi3.Init.CLKPhase = SPI_PHASE_1EDGE;
	hspi3.Init.CLKPolarity = SPI_POLARITY_LOW;
	hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
	hspi3.Init.CRCPolynomial = 7;
	hspi3.Init.DataSize = SPI_DATASIZE_8BIT;
	hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
	hspi3.Init.NSS = SPI_NSS_SOFT;
	hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
	hspi3.Init.Mode = SPI_MODE_MASTER;
	if (HAL_SPI_Init(&hspi3) != HAL_OK) {
		return;
	}

	////

	tiradio->reset_n.port = GPIOG;
	tiradio->reset_n.pin = GPIO_PIN_2;

	tiradio->cs_n.port = GPIOC;
	tiradio->cs_n.pin = GPIO_PIN_8;

	tiradio->gpio0.port = GPIOD;
	tiradio->gpio0.pin = GPIO_PIN_2;
	tiradio->gpio0.irq_no = EXTI2_IRQn;

	tiradio->gpio2.port = GPIOC;
	tiradio->gpio2.pin = GPIO_PIN_9;
	tiradio->gpio2.irq_no = EXTI9_5_IRQn;

	tiradio->gpio3.port = GPIOG;
	tiradio->gpio3.pin = GPIO_PIN_3;
	tiradio->gpio3.irq_no = EXTI3_IRQn;

	tiradio->spi = &hspi3;

	if (tiradio_init(tiradio) != TIRADIO_ERR_OK) {
		return;
	}
	_g_tiradio = tiradio;

	if (tiradio_reset(tiradio) != TIRADIO_ERR_OK) {
		return;
	}
}

#else

#error "Unsupported UBINOS__BSP__BOARD_MODEL"

#endif

int appmain(int argc, char *argv[]) {
	int r;

	__enable_irq();
	ARM_INTERRUPT_ENABLE();

	HAL_Init();

	ARM_INTERRUPT_DISABLE();
	__disable_irq();

	r = task_create(NULL, root_task, NULL, task_getmiddlepriority(), 0, "root_task");
	if (0 != r) {
		logme("fail at task_create\n\r");
	}

	ubik_comp_start();

	return 0;
}

static void root_task(void *arg) {
	int r;
	tiradio_test_params_t *params = &_tiradio_test_params;
	tiradio_t *tiradio = &_tiradio;

	radio_init();

	printf("\n\n\r\n");
	printf("================================================================================\r\n");
	printf("tiradio_carrier_tester (build time: %s %s)\r\n", __TIME__, __DATE__);
	printf("================================================================================\r\n");
	printf("\r\n");

	srand(time(NULL));

	dtty_setecho(1);

	tiradio_phy_detect(tiradio);

	params->tiradio = &_tiradio;

#if 0
	params->preset_index = 21;
	params->packet_size = 30;
	params->enable_phy_ack = 0;
	params->enable_phy_wor = 0;
	params->enable_phy_wor_auto_calibration = 0;
	params->wor_event0 = 0x02E9;
	params->wor_event1 = 0x7;
	params->wor_resolution = 0;
	params->tx_power_index = 1;
	params->tx_interval_ms = 0;
	params->test_index = 0;
#else
	params->preset_index = 21;
	params->packet_size = 6;
	params->enable_phy_ack = 0;
	params->enable_phy_wor = 1;
	params->enable_phy_wor_auto_calibration = 1;
	params->wor_event0 = 40000;
	params->wor_event1 = 0x7;
	params->wor_resolution = 0;
	params->tx_power_index = 1;
	params->tx_interval_ms = 1000;
	params->test_index = 11;
#endif

	r = task_create(NULL, carrier_test_main, NULL, task_getmiddlepriority(), 0, "carrier_test_main");
	if (0 != r) {
		logme("fail at task_create\n\r");
	}

#if carrier_TESTER_TX
	r = task_create(NULL, carrier_test_cmd_main, NULL, task_getmiddlepriority(), 0, "carrier_test_cmd_main");
	if (0 != r) {
		logme("fail at task_create\n\r");
	}
#endif
}

void init_phy_sniff_mode(tiradio_t *tiradio) {
	tiradio_err_t err = TIRADIO_ERR_ERROR;

	tiradio_test_params_t params;
	params.preset_index = 21;
	params.enable_phy_ack = 0;
	params.enable_phy_wor = 1;
	params.enable_phy_wor_auto_calibration = 0;
	params.wor_event0 = 745;
	params.wor_event1 = 7;
	params.wor_resolution = 0;
	params.tx_power_index = 1;
	params.tx_interval_ms = 1000;

	do {
		err = tiradio_reset(tiradio);
		if (err != TIRADIO_ERR_OK) {
			printf("fail at tiradio_reset\n\r");
			break;
		}
		err = tiradio_phy_detect(tiradio);
		if (err != TIRADIO_ERR_OK) {
			printf("fail at tiradio_phy_detect\n\r");
			break;
		}
		err = tiradio_phy_config_set_preset(tiradio, params.preset_index);
		if (err != TIRADIO_ERR_OK) {
			printf("fail at tiradio_phy_config_set_preset\n\r");
			break;
		}
		err = tiradio_phy_config_set_enable_phy_ack(tiradio, params.enable_phy_ack);
		if (err != TIRADIO_ERR_OK) {
			printf("fail at tiradio_phy_config_set_enable_phy_ack\n\r");
			break;
		}
		err = tiradio_phy_config_set_enable_phy_wor(tiradio, params.enable_phy_wor);
		if (err != TIRADIO_ERR_OK) {
			printf("fail at tiradio_phy_config_set_enable_phy_wor\n\r");
			break;
		}
		err = tiradio_phy_config_set_enable_phy_wor_auto_calibration(tiradio, params.enable_phy_wor_auto_calibration);
		if (err != TIRADIO_ERR_OK) {
			printf("fail at tiradio_phy_config_set_enable_phy_wor_auto_calibration\n\r");
			break;
		}
		err = tiradio_phy_config_set_wor_event0(tiradio, params.wor_event0);
		if (err != TIRADIO_ERR_OK) {
			printf("fail at tiradio_phy_config_set_wor_event0\n\r");
			break;
		}
		err = tiradio_phy_config_set_wor_event1(tiradio, params.wor_event1);
		if (err != TIRADIO_ERR_OK) {
			printf("fail at tiradio_phy_config_set_wor_event1\n\r");
			break;
		}
		err = tiradio_phy_config_set_wor_resolution(tiradio, params.wor_resolution);
		if (err != TIRADIO_ERR_OK) {
			printf("fail at tiradio_phy_config_set_wor_resolution\n\r");
			break;
		}
		err = tiradio_phy_config_set_tx_power(tiradio, params.tx_power_index);
		if (err != TIRADIO_ERR_OK) {
			printf("fail at tiradio_phy_config_set_tx_power\n\r");
			break;
		}
		err = tiradio_phy_config_apply(tiradio);
		if (err != TIRADIO_ERR_OK) {
			printf("fail at tiradio_phy_config_apply\n\r");
			break;
		}

		printf("chip: id = 0x%02x, version = 0x%02x\r\n", tiradio->chip.id, tiradio->chip.ver);

	} while (0);

	tiradio->err = err;
}

void carrier_test_config(tiradio_t *tiradio, tiradio_carrier_config_t *carrier_config) {
	carrier_config->tiradio = tiradio;
	tiradio_assert(carrier_config->tiradio != NULL);

	carrier_config->my_addr[0] = TEST_carrier_ADDRESS0;
	carrier_config->my_addr[1] = TEST_carrier_ADDRESS1;
	carrier_config->my_addr[2] = TEST_carrier_ADDRESS2;
	carrier_config->my_addr[3] = TEST_carrier_ADDRESS3;
	carrier_config->my_addr[4] = TEST_carrier_ADDRESS4;
	carrier_config->my_addr[5] = TEST_carrier_ADDRESS5;

	carrier_config->addr_unit = TEST_carrier_ADDR_UNIT;
}

void print_packet(tiradio_carrier_packet_t *carrier_packet) {
	printf("type = %d, ", carrier_packet->type);
	printf("src_addr = %02x:%02x:%02x:%02x:%02x:%02x, ", carrier_packet->src_addr[0], carrier_packet->src_addr[1], carrier_packet->src_addr[2],
			carrier_packet->src_addr[3], carrier_packet->src_addr[4], carrier_packet->src_addr[5]);
	printf("dst_addr = %02x:%02x:%02x:%02x:%02x:%02x, ", carrier_packet->dst_addr[0], carrier_packet->dst_addr[1], carrier_packet->dst_addr[2],
			carrier_packet->dst_addr[3], carrier_packet->dst_addr[4], carrier_packet->dst_addr[5]);
	printf("seq_no = %d, direction = %d", carrier_packet->seq_no, carrier_packet->direction);
	if (carrier_packet->msg_len != 0){
		printf(", msg_len: %d\r\n", carrier_packet->msg_len);
		printf("net_type = %d, ", carrier_packet->msg[TEST_NET_INDEX_PACKET_TYPE]);
		printf("net_src_addr = %02x:%02x:%02x:%02x, ", carrier_packet->msg[TEST_NET_INDEX_SRC_ADDR_BASE], carrier_packet->msg[TEST_NET_INDEX_SRC_ADDR_BASE + 1],
				carrier_packet->msg[TEST_NET_INDEX_SRC_ADDR_BASE + 2], carrier_packet->msg[TEST_NET_INDEX_SRC_ADDR_BASE + 3]);
		printf("net_dst_addr = %02x:%02x:%02x:%02x, ", carrier_packet->msg[TEST_NET_INDEX_DST_ADDR_BASE], carrier_packet->msg[TEST_NET_INDEX_DST_ADDR_BASE + 1],
						carrier_packet->msg[TEST_NET_INDEX_DST_ADDR_BASE + 2], carrier_packet->msg[TEST_NET_INDEX_DST_ADDR_BASE + 3]);
		printf("net_data: %s\r\n", &carrier_packet->msg[TEST_NET_INDEX_DATA_BASE]);
	}
	else
		printf("\r\n");
}

void processing_carrier_packet(tiradio_carrier_config_t *carrier_config, tiradio_carrier_packet_t *rx_packet) {
	tiradio_err_t err;

	if (rx_packet->type == carrier_PACKET_TYPE_ACK) {
//		tickcount_carrier_ack = ubik_gettickcount();
//		tickdiff1 = ubik_gettickdiff(tickcount_tx, tickcount_carrier_ack);
//		printf("received ACK, RTT = %ums\r\n", ubik_ticktotimems(tickdiff1.low));
	} else {
		//ack packet tx delay
		task_sleepms(TEST_carrier_ACK_DELAY_MS);

		//send ACK msg
		err = carrier_packet_send_ack(carrier_config, rx_packet);
		if (err != TIRADIO_ERR_OK) {
			printf("tiradio_err (%d) occured\r\n", err);
			return;
		}
		task_sleepms(carrier_MODE_SWITCH_DELAY_MS);	//switching delay between tx/rx

		print_packet(rx_packet);

		//check carrier/GENERAL PACKET
		if (rx_packet->type == carrier_PACKET_TYPE_carrier) {
			//Forward if carrier packet
			err = carrier_packet_forward(carrier_config, rx_packet);
			if (err != TIRADIO_ERR_OK) {
				printf("tiradio_err (%d) occured\r\n", err);
				return;
			}

//			tickcount_forward = ubik_gettickcount();
			printf("packet Forward\r\n");

			task_sleepms(carrier_MODE_SWITCH_DELAY_MS);	//switching delay between tx/rx

		} else {
			//GENERAL PACKET
		}
	}
}

void processing_net_packet(tiradio_carrier_config_t* carrier_config, tiradio_carrier_packet_t* rx_packet) {
	int i;
	tiradio_err_t err;
	tiradio_carrier_packet_t tx_packet;

	if (rx_packet->type == carrier_PACKET_TYPE_ACK) {
//		tickcount_carrier_ack = ubik_gettickcount();
//		tickdiff1 = ubik_gettickdiff(tickcount_tx, tickcount_carrier_ack);
//		printf("received ACK, RTT = %ums\r\n", ubik_ticktotimems(tickdiff1.low));
	} else if (rx_packet->type == carrier_PACKET_TYPE_GENERAL || rx_packet->type == carrier_PACKET_TYPE_carrier) {
		//ack packet tx delay
		task_sleepms(TEST_carrier_ACK_DELAY_MS);

		//send ACK msg
		err = carrier_packet_send_ack(carrier_config, rx_packet);
		if (err != TIRADIO_ERR_OK) {
			printf("tiradio_err (%d) occured\r\n", err);
			return;
		}
		task_sleepms(carrier_MODE_SWITCH_DELAY_MS);	//switching delay between tx/rx

		print_packet(rx_packet);

		//process net packet
		if (memcmp(&rx_packet->msg[TEST_NET_INDEX_DST_ADDR_BASE], test_net_my_addr, TEST_NET_ADDR_LEN) == 0) {
			if (rx_packet->msg[TEST_NET_INDEX_PACKET_TYPE] == TEST_NET_PACKET_TYPE_ACK) {
				tickcount_net_ack = ubik_gettickcount();
				tickdiff2 = ubik_gettickdiff(tickcount_tx, tickcount_net_ack);
				printf("received network ACK, RTT = %ums\r\n", ubik_ticktotimems(tickdiff2.low));
			} else if (rx_packet->msg[TEST_NET_INDEX_PACKET_TYPE] == TEST_NET_PACKET_TYPE_GENERAL) {
				//generate tx packet
				tx_packet.type = carrier_PACKET_TYPE_carrier;
				memcpy(tx_packet.src_addr, carrier_config->my_addr, carrier_ADDR_LEN);
				memcpy(tx_packet.dst_addr, rx_packet->src_addr, carrier_ADDR_LEN);
				tx_packet.seq_no = rx_packet->seq_no;
				tx_packet.direction = carrier_DIR_BACKWARD;
				tx_packet.msg_len = TEST_carrier_MSG_LEN;

				tx_packet.msg[TEST_NET_INDEX_PACKET_TYPE] = TEST_NET_PACKET_TYPE_ACK;
				memcpy(&tx_packet.msg[TEST_NET_INDEX_SRC_ADDR_BASE], &test_net_my_addr, TEST_NET_ADDR_LEN);
				memcpy(&tx_packet.msg[TEST_NET_INDEX_DST_ADDR_BASE], &rx_packet->msg[TEST_NET_INDEX_SRC_ADDR_BASE], TEST_NET_ADDR_LEN);

				for (i = TEST_NET_INDEX_DATA_BASE; i < TEST_carrier_MSG_LEN; i++) {
					tx_packet.msg[i] = 0;
				}

				err = carrier_packet_tx(carrier_config, &tx_packet);
				if (err != TIRADIO_ERR_OK) {
					printf("tiradio_err (%d) occured\r\n", err);
					return;
				}

				printf("net ACK TX\r\n");

				task_sleepms(carrier_MODE_SWITCH_DELAY_MS);	//switching delay between tx/rx
			}
		} else {
			//check carrier/GENERAL PACKET
			if (rx_packet->type == carrier_PACKET_TYPE_carrier) {
				//Forward if carrier packet
				err = carrier_packet_forward(carrier_config, rx_packet);
				if (err != TIRADIO_ERR_OK) {
					printf("tiradio_err (%d) occured\r\n", err);
					return;
				}

	//			tickcount_forward = ubik_gettickcount();
				printf("packet Forward\r\n");

				task_sleepms(carrier_MODE_SWITCH_DELAY_MS);	//switching delay between tx/rx

			} else {
				//GENERAL PACKET
			}
		}
	}
}

void carrier_test_packet_send(tiradio_carrier_config_t *carrier_config, carrier_packet_type_t type, uint16_t seq_no) {
	int i;
	tiradio_err_t err;
	tiradio_carrier_packet_t tx_packet;
	uint8_t dst_addr[carrier_ADDR_LEN];

	//패킷을 보낼 디바이스 주소
	memcpy(dst_addr, carrier_config->my_addr, carrier_ADDR_LEN);
	dst_addr[5] += carrier_config->addr_unit;

	//generate tx packet
	tx_packet.type = type;
	memcpy(tx_packet.src_addr, carrier_config->my_addr, carrier_ADDR_LEN);
	memcpy(tx_packet.dst_addr, dst_addr, carrier_ADDR_LEN);
	tx_packet.seq_no = seq_no;
	tx_packet.direction = carrier_DIR_FORWARD;
	tx_packet.msg_len = TEST_carrier_MSG_LEN;

	tx_packet.msg[TEST_NET_INDEX_PACKET_TYPE] = TEST_NET_PACKET_TYPE_GENERAL;
	memcpy(&tx_packet.msg[TEST_NET_INDEX_SRC_ADDR_BASE], test_net_my_addr, TEST_NET_ADDR_LEN);
	memcpy(&tx_packet.msg[TEST_NET_INDEX_DST_ADDR_BASE], test_net_dst_addr, TEST_NET_ADDR_LEN);
	for (i = TEST_NET_INDEX_DATA_BASE; i < TEST_carrier_MSG_LEN; i++) {
		tx_packet.msg[i] = (uint8_t) '!' + (rand() % 94);
	}

	err = carrier_packet_tx(carrier_config, &tx_packet);
	if (err != TIRADIO_ERR_OK) {
		printf("tiradio_err (%d) occured\r\n", err);
		return;
	}

	tickcount_tx = ubik_gettickcount();
	printf("packet TX\r\n");

	task_sleepms(carrier_MODE_SWITCH_DELAY_MS);	//switching delay between tx/rx
}

void carrier_test_main(void *arg) {
	tiradio_err_t err;
	tiradio_carrier_config_t carrier_config;
	tiradio_t *tiradio = &_tiradio;
	uint16_t seq = 0;
	tiradio_carrier_packet_t rx_packet;

	printf("carrier_test_main start\r\n\n");
	//init sniff mode
	init_phy_sniff_mode(tiradio);

	carrier_test_config(tiradio, &carrier_config);

	//main loop
	while (1) {
		//Idle 상태는 RX 모드 유지
		rx_state = true;
		err = carrier_packet_rx(&carrier_config, &rx_packet, TEST_carrier_MSG_LEN);
		printf("err : %d, rx_packet[%d] : %s", err, rx_packet.msg_len, rx_packet.msg );
		rx_state = false;
		switch (err) {
		case TIRADIO_ERR_OK:
//			processing_carrier_packet(&carrier_config, &rx_packet);
			processing_net_packet(&carrier_config, &rx_packet);
			break;

		case TIRADIO_ERR_CANCLE:
			//tiradio_ctl CANCEL 명령이 실행되면 carrier_packet_rx에서 TIRADIO_ERR_CANCEL값을 반환
			tiradio_ctl(carrier_config.tiradio, TIRADIO_CTL_CMD_NONE); //CANCEL 명령 초기화
//			carrier_test_packet_send(&carrier_config, carrier_PACKET_TYPE_GENERAL, seq);
			carrier_test_packet_send(&carrier_config, carrier_PACKET_TYPE_carrier, seq);
			seq++;
			break;

		case TIRADIO_ERR_CARRIER_ADDR_MISMATCH:
			//dst_addr이 내 주소가 아닌 패킷 처리
			break;

		default:
			printf("tiradio_err (%d) occured\r\n", err);
			break;
		}

	}
}

void carrier_test_cmd_main(void *arg) {
	tiradio_t *tiradio = &_tiradio;

	task_sleepms(1000);

	while (1) {
		if (rx_state) {
			//주기적으로 ctl CANCEL 명령을 내려 TX 수행
			tiradio_ctl(tiradio, TIRADIO_CTL_CMD_CANCLE);
			task_sleepms(TEST_TX_PERIOD_MS);
		} else {
			task_sleepms(10);
		}
	}
}

#endif /* (INCLUDE__APP__tiradio_mac_carrier_tester == 1) */

