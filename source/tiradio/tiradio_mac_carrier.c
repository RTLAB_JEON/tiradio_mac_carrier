/*
 * tiradio_carrier.c
 *
 *  Created on: 2019. 11. 21.
 *      Author: chlee
 */

#include <stdio.h>
#include <string.h>
#include <tiradio.h>
#include <tiradio/phy.h>
#include <tiradio_mac_carrier.h>

uint64_t addr_to_uint64(uint8_t *arr) {
	uint64_t address = 0;

	address += (((uint64_t) arr[5] << 0) & 0x00000000000000FF);
	address += (((uint64_t) arr[4] << 8) & 0x000000000000FF00);
	address += (((uint64_t) arr[3] << 16) & 0x0000000000FF0000);
	address += (((uint64_t) arr[2] << 24) & 0x00000000FF000000);
	address += (((uint64_t) arr[1] << 32) & 0x000000FF00000000);
	address += (((uint64_t) arr[0] << 40) & 0x0000FF0000000000);

	return address;
}

void uint64_to_addr(uint64_t address, uint8_t *arr) {
	arr[5] = (uint8_t) ((address >> 0) & 0xFF);
	arr[4] = (uint8_t) ((address >> 8) & 0xFF);
	arr[3] = (uint8_t) ((address >> 16) & 0xFF);
	arr[2] = (uint8_t) ((address >> 24) & 0xFF);
	arr[1] = (uint8_t) ((address >> 32) & 0xFF);
	arr[0] = (uint8_t) ((address >> 40) & 0xFF);
}

tiradio_err_t carrier_packet_rx(tiradio_carrier_config_t *carrier_config, tiradio_carrier_packet_t *carrier_packet, uint8_t carrier_msg_len) {
	tiradio_err_t err = TIRADIO_ERR_ERROR;
	tiradio_t *tiradio = carrier_config->tiradio;
	tiradio_packet_t packet;

	//packet setting
	packet.size = carrier_HEADER_LEN + carrier_msg_len;
	packet.wor_capture = 0;

	//Packet RX : sniff mode(blocking)
	err = tiradio_phy_recv(tiradio, &packet);
	//check RX complete
	if (err != TIRADIO_ERR_OK)
		return err;

	//copy rx data to rx packet
	carrier_packet->type = packet.buf[carrier_INDEX_TYPE];
	memcpy(carrier_packet->src_addr, &packet.buf[carrier_INDEX_SRC_ADDR_BASE], carrier_ADDR_LEN);
	memcpy(carrier_packet->dst_addr, &packet.buf[carrier_INDEX_DST_ADDR_BASE], carrier_ADDR_LEN);
	carrier_packet->seq_no = (((uint16_t) packet.buf[carrier_INDEX_SEQ_NO_HIGH]) << 8 | ((uint16_t) packet.buf[carrier_INDEX_SEQ_NO_LOW]));
	carrier_packet->direction = packet.buf[carrier_INDEX_carrier_DIR];

	carrier_packet->msg_len = packet.buf[carrier_INDEX_MSG_LEN];

	if (carrier_packet->msg_len <= carrier_msg_len) {
		if (carrier_packet->msg_len != 0)
			memcpy(carrier_packet->msg, &packet.buf[carrier_INDEX_MSG_BASE], carrier_packet->msg_len);
	} else {
		return TIRADIO_ERR_PACKET_ERROR;
	}
	//check dst address
	if (memcmp(carrier_config->my_addr, &carrier_packet->dst_addr, carrier_ADDR_LEN) != 0) {
		//not matched address
		return TIRADIO_ERR_CARRIER_ADDR_MISMATCH;
	}

	//rx complete
	return err;
}

tiradio_err_t carrier_packet_tx(tiradio_carrier_config_t *carrier_config, tiradio_carrier_packet_t *carrier_packet) {
	//send carrier packet
	tiradio_err_t err = TIRADIO_ERR_ERROR;
	tiradio_t *tiradio = carrier_config->tiradio;
	tiradio_packet_t packet;

	packet.size = carrier_HEADER_LEN + carrier_packet->msg_len;

	packet.buf[carrier_INDEX_TYPE] = carrier_packet->type;
	memcpy(&packet.buf[carrier_INDEX_SRC_ADDR_BASE], carrier_config->my_addr, carrier_ADDR_LEN);
	memcpy(&packet.buf[carrier_INDEX_DST_ADDR_BASE], carrier_packet->dst_addr,
	carrier_ADDR_LEN);
	packet.buf[carrier_INDEX_SEQ_NO_LOW] = (uint8_t) ((carrier_packet->seq_no >> 0) & 0xFF);
	packet.buf[carrier_INDEX_SEQ_NO_HIGH] = (uint8_t) ((carrier_packet->seq_no >> 8) & 0xFF);
	packet.buf[carrier_INDEX_carrier_DIR] = carrier_packet->direction;
	packet.buf[carrier_INDEX_MSG_LEN] = carrier_packet->msg_len;
	if (carrier_packet->msg_len != 0) {
		memcpy(&packet.buf[carrier_INDEX_MSG_BASE], carrier_packet->msg, carrier_packet->msg_len);
	}

	err = tiradio_phy_send(tiradio, &packet);

	return err;
}

tiradio_err_t carrier_packet_send_ack(tiradio_carrier_config_t *carrier_config, tiradio_carrier_packet_t *carrier_received_packet) {
	int i;
	tiradio_err_t err = TIRADIO_ERR_ERROR;
	tiradio_t *tiradio = carrier_config->tiradio;
	tiradio_packet_t packet;

	packet.size = carrier_HEADER_LEN + carrier_received_packet->msg_len;;

	packet.buf[carrier_INDEX_TYPE] = carrier_PACKET_TYPE_ACK;
	memcpy(&packet.buf[carrier_INDEX_SRC_ADDR_BASE], carrier_config->my_addr, carrier_ADDR_LEN);
	memcpy(&packet.buf[carrier_INDEX_DST_ADDR_BASE], carrier_received_packet->src_addr, carrier_ADDR_LEN);
	packet.buf[carrier_INDEX_SEQ_NO_LOW] = (uint8_t) ((carrier_received_packet->seq_no >> 0) & 0xFF);
	packet.buf[carrier_INDEX_SEQ_NO_HIGH] = (uint8_t) ((carrier_received_packet->seq_no >> 8) & 0xFF);
	packet.buf[carrier_INDEX_carrier_DIR] = carrier_received_packet->direction;
	packet.buf[carrier_INDEX_MSG_LEN] = 0;
	for (i = carrier_INDEX_MSG_BASE; i < packet.size; i++) {
		packet.buf[i] = 0;
	}

	err = tiradio_phy_send(tiradio, &packet);

	return err;
}

tiradio_err_t carrier_packet_forward(tiradio_carrier_config_t *carrier_config, tiradio_carrier_packet_t *carrier_received_packet) {
	//send carrier packet
	tiradio_err_t err = TIRADIO_ERR_ERROR;
	tiradio_t *tiradio = carrier_config->tiradio;
	tiradio_packet_t packet;
	uint64_t address = addr_to_uint64(carrier_config->my_addr);

	//change dst address to the next device
	if (carrier_received_packet->direction == carrier_DIR_FORWARD)
		address += carrier_config->addr_unit;
	else if (carrier_received_packet->direction == carrier_DIR_BACKWARD)
		address -= carrier_config->addr_unit;
	uint64_to_addr(address, &packet.buf[carrier_INDEX_DST_ADDR_BASE]);

	packet.size = carrier_HEADER_LEN + carrier_received_packet->msg_len;

	packet.buf[carrier_INDEX_TYPE] = carrier_received_packet->type;
	memcpy(&packet.buf[carrier_INDEX_SRC_ADDR_BASE], carrier_config->my_addr, carrier_ADDR_LEN);
	packet.buf[carrier_INDEX_SEQ_NO_LOW] = (uint8_t) ((carrier_received_packet->seq_no >> 0) & 0xFF);
	packet.buf[carrier_INDEX_SEQ_NO_HIGH] = (uint8_t) ((carrier_received_packet->seq_no >> 8) & 0xFF);
	packet.buf[carrier_INDEX_carrier_DIR] = carrier_received_packet->direction;
	packet.buf[carrier_INDEX_MSG_LEN] = carrier_received_packet->msg_len;
	if (carrier_received_packet->msg_len != 0) {
		memcpy(&packet.buf[carrier_INDEX_MSG_BASE], carrier_received_packet->msg, carrier_received_packet->msg_len);
	}

	err = tiradio_phy_send(tiradio, &packet);

	return err;
}

